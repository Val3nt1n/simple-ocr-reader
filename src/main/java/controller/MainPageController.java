package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainPageController implements Initializable {
    public JFXButton loadImage;
    public Button readOCR;
    public TextField textFiel;
    public JFXTextArea textArea;

    final FileChooser fileChooser = new FileChooser();
    public JFXButton close;
    public AnchorPane anchorPane;
    private Desktop desktop = Desktop.getDesktop();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        close.setOnAction(t -> System.exit(0));

    }


    public void readOcrImage(ActionEvent actionEvent) throws TesseractException {
        String file = textFiel.getText();
        Tesseract tesseract = new Tesseract();
        tesseract.setDatapath("C:\\Program Files\\Tesseract-OCR\\tessdata");
        String fullText = tesseract.doOCR(new File(file));
        textArea.setText(fullText);
    }

    private void openFile(File file) throws IOException {
        desktop.open(file);
    }

    public void getImage(ActionEvent actionEvent) throws IOException {
        FileChooser fileChooser = new FileChooser();
        loadImage.setOnAction(e -> {
            fileChooser.setTitle("Load image to read!");
            Stage stage = (Stage) anchorPane.getScene().getWindow();
            File selectedFile = fileChooser.showOpenDialog(stage);
            textFiel.setText(selectedFile.getAbsolutePath());
        });
    }
}
